var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    rename = require('gulp-rename');
var autoprefixer = require('gulp-autoprefixer');
var babel = require('gulp-babel');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin'),
    cache = require('gulp-cache');
var sass = require('gulp-sass');
var browserSync = require('browser-sync'),
    browserify = require('browserify'),
    babelify = require('babelify');
var source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    notifier = require('node-notifier'),
    gutil = require('gulp-util'),
    nodemon = require('gulp-nodemon');

// Standard handler
function standardHandler(err) {
  notifier.notify({
    'title': 'Error',
    'message': err.message
  });
  gutil.log(gutil.colors.red('Error'), err.message);
}

// Error handler for Plumber
var onError = function (err) {
  gutil.beep();
  console.log(err);
};

// Error handler for Browserify
function browserifyHandler(err) {
  standardHandler(err);
  this.emit('end');
}

gulp.task('nodemon', function (cb) {
  var called = false;
  return nodemon({
    script: 'backend/BEMain.js',
    ignore: [
      'gulpfile.js',
      'node_modules/'
    ]
  })
    .on('start', function () {
      if (!called) {
        called = true;
        cb();
      }
    })
});

gulp.task('browser-sync', ['nodemon'], function() {
  browserSync({
    server: {
      proxy: "localhost:3000",  // local node app address
      port: 5000,  // use *different* port than above
      notify: true
    }
  });
});

gulp.task('bs-reload', ['scripts'], function () {
  browserSync.reload();
});

gulp.task('images', function(){
  gulp.src('app/images/**/*')
    .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
    .pipe(gulp.dest('dist/images/'));
});

gulp.task('styles', function(){
  gulp.src(['app/styles/**/*.scss'])
    .pipe(plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
    }}))
    .pipe(sass())
    .pipe(autoprefixer('last 2 versions'))
    .pipe(gulp.dest('dist/styles/'))
    .pipe(browserSync.reload({stream:true}))
});

gulp.task('copyfonts', function() {
  gulp.src([
    'app/fonts/**/*',
    'bower_components/ionicons/fonts/*'
  ])
    .pipe(gulp.dest('dist/fonts'));
});

gulp.task('vendor-scripts', function() {
  return gulp.src([
    'bower_components/jquery/dist/jquery.min.js',
    'bower_components/scrollmagic/scrollmagic/minified/ScrollMagic.min.js',
    'bower_components/gsap/src/minified/TweenMax.min.js',
    'bower_components/scrollmagic/scrollmagic/minified/plugins/animation.gsap.min.js'
  ])
    .pipe(plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
    }}))
    .pipe(concat('vendor.js'))
    .pipe(gulp.dest('dist/scripts/'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('dist/scripts/'))
    .pipe(browserSync.reload({stream:true}))
});

gulp.task('scripts', function(){
  var bundler = browserify('./app/scripts/app.js', {basedir: __dirname, debug: true});
  bundler.transform(babelify.configure({
    optional: ["es7.objectRestSpread"]
  }));

  var stream = bundler.bundle();

  return stream
    .on('error', browserifyHandler)
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(source('main.js'))
    .pipe(buffer())
    .pipe(gulp.dest('dist/scripts/'))
    .pipe(browserSync.reload({stream:true}))
});

gulp.task('default', ['browser-sync'], function(){
  gulp.watch("app/styles/**/*.scss", ['styles']);
  gulp.watch("app/scripts/**/*", ['scripts']);
  gulp.watch("*.html", ['bs-reload']);
});
