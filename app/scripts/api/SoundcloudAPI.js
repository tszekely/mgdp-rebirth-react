import APP_CONSTANTS from '../constants/AppConstants';
const API = APP_CONSTANTS.API.SOUNDCLOUD;

const SoundcloudAPI = {
  authenticate() {
    SC.initialize({
      client_id: APP_CONSTANTS.CLIENT_ID,
      redirect_uri: "https://developers.soundcloud.com/callback.html"
    });

    SC.stream("/tracks/293", function(sound){
      console.log(sound);
    });

    SC.get('/me', [], (response)=>{
      console.log(response);
    });
  },

  loadAll () {

  }
};

export default SoundcloudAPI;
