import APP_CONSTANTS from '../constants/AppConstants';
const API = APP_CONSTANTS.API.SONGS;

import SongActions from '../actions/server/SongActions.js';

const SongAPI = {
  loadAll () {
    return $.ajax({
      url: API.GET_SONGS,
      type: 'GET',
      dataType: 'json'
    })
      .done((response)=>{
        SongActions.loadAll(response);
      });
  },

  addSong (song) {
    return $.ajax({
      url: API.ADD_SONG,
      type: 'POST',
      dataType: 'json',
      contentType: 'application/json',
      data: JSON.stringify(song)
    })
      .done((response)=>{
        SongActions.loadAll(response);
      });
  },


  deleteSong (songId) {
    return $.ajax({
      url: API.DELETE_SONG.replace('${song_id}', songId),
      type: 'DELETE',
      dataType: 'json',
      contentType: 'application/json'
    })
      .done((response)=>{
        SongActions.loadAll(response);
      });
  }
};

export default SongAPI;
