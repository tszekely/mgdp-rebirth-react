/*
 * A singleton that operates as the central hub for application updates.
 */
import { Dispatcher } from 'flux';

import assign from 'object-assign';
import Constants from '../constants/AppConstants';

const AppDispatcher = assign(new Dispatcher(), {

  /**
   * A bridge function between the views and the dispatcher, marking the action
   * as a view action.  Another variant here could be handleServerAction.
   * @param  {action} action The data coming from the view.
   */
    handleViewAction (action) {
    console.info('View action:', action.actionType, action);

    if (!action.actionType) {
      throw new Error('Empty action.type: you likely mistyped the action.');
    }

    this.dispatch({
      source: Constants.ActionSources.VIEW_ACTION,
      action: action
    });
  },

  /**
   * A bridge function between the server and the dispatcher, marking the action
   * as a server action.
   * @param  {action} action The data coming from the server.
   */
    handleServerAction (action) {
    console.info('Server action:', action.actionType, action);

    if (!action.actionType) {
      throw new Error('Empty action.type: you likely mistyped the action.');
    }

    this.dispatch({
      source: Constants.ActionSources.SERVER_ACTION,
      action: action
    });
  }

});

export default AppDispatcher;
