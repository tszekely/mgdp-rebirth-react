import AppDispatcher from '../dispatcher/AppDispatcher';
import Events from 'events';
const { EventEmitter } = Events;
import ACTION_TYPES from '../constants/ActionTypes';
import assign from 'object-assign';

var _initialized,
  _songs = [];

const CHANGE_EVENT = 'change';

const SongStore = assign({}, EventEmitter.prototype, {
  getAll () {
    return _songs;
  },
  loadAll (json) {
    _songs = json;
    this.emitChange();
  },
  emitChange () {
    this.emit(CHANGE_EVENT);
  },
  addChangeListener (callback) {
    this.on(CHANGE_EVENT, callback);
  },
  removeChangeListener (callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }
});

SongStore.dispatcherIndex = AppDispatcher.register(
  function(payload) {
    var action = payload.action;

    switch(action.actionType){
      case ACTION_TYPES.SONGS.SONGS_LOADED:
        SongStore.loadAll(action.data);
        break;
    }
  }
);

export default SongStore;
