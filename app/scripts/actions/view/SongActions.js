import AppDispatcher from '../../dispatcher/AppDispatcher';
import ACTION_TYPES from '../../constants/ActionTypes';

import SongAPI from '../../api/SongAPI';

const SongActions = {
  loadAll () {
    AppDispatcher.handleViewAction({
      actionType: ACTION_TYPES.SONGS.GET_SONGS
    });
    return SongAPI.loadAll();
  },

  addSong (song) {
    AppDispatcher.handleViewAction({
      actionType: ACTION_TYPES.SONGS.ADD_SONG,
      data: song
    });
    return SongAPI.addSong(song);
  },

  deleteSong (songId) {
    AppDispatcher.handleViewAction({
      actionType: ACTION_TYPES.SONGS.DELETE_SONG,
      data: songId
    });
    return SongAPI.deleteSong(songId);
  }
};

export default SongActions;
