import AppDispatcher from '../../dispatcher/AppDispatcher';
import ACTION_TYPES from '../../constants/ActionTypes';

const SongActions = {
  loadAll (data) {
    AppDispatcher.handleServerAction({
      actionType: ACTION_TYPES.SONGS.SONGS_LOADED,
      data: data
    });
  }
};

export default SongActions;
