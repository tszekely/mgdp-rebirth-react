import config from '../environment-config.json';
import keyMirror from 'react/lib/keyMirror';

const SOUNDCLOUD_API_BASE_URL = config.SOUNDCLOUD_API_BASE_URL,
  ENVIRONMENT = config.ENVIRONMENT,
  CLIENT_ID = config.CLIENT_ID,
  CLIENT_SECRET = config.CLIENT_SECRET,
  API_BASE_URL = config.API_BASE_URL;

const APP_CONSTANTS = {
  ENVIRONMENT: ENVIRONMENT,
  CLIENT_ID: CLIENT_ID,
  CLIENT_SECRET: CLIENT_SECRET,
  API_BASE_URL: API_BASE_URL,

  ActionSources: keyMirror({
    SERVER_ACTION: null,
    VIEW_ACTION: null
  }),

  API: {
    SOUNDCLOUD: {
      GET_ALL: SOUNDCLOUD_API_BASE_URL
    },

    SONGS: {
      GET_SONGS: API_BASE_URL + '/songs',               // GET
      ADD_SONG: API_BASE_URL + '/songs',                // POST,
      DELETE_SONG: API_BASE_URL + '/songs/${song_id}'   // DELETE
    }
  }
};

export default APP_CONSTANTS;
