'use strict';

import keyMirror from 'react/lib/keyMirror';

const ACTION_TYPES = {
  GLOBAL: keyMirror({
    INITIALIZE: null,
    NOTIFICATION_RECEIVED: null
  }),

  SONGS: keyMirror({
    GET_SONGS: null,
    SONGS_LOADED: null,
    ADD_SONG: null,
    SONG_ADDED: null,
    DELETE_SONG: null
  }),

  USER: keyMirror({
    GET_USER_REGISTRATION: null,
    UPDATE_USER_REGISTRATION: null,
    UPDATE_USER_PASSWORD: null,
    UNAUTHORIZED: null,
    LOGOUT: null,
    SET_USER_PREFERENCES: null
  })
};

export default ACTION_TYPES;
