import React from 'react';
import Router from 'react-router';
let {RouteHandler} = Router;

//import Firebase from 'firebase';
//import ReactFireMixin from 'reactfire';

import Header from './common/Header.jsx';

//import SongAPI from '../api/SongAPI.js';
import SongActions from '../actions/view/SongActions.js';
import SongStore from '../stores/SongStore.js';

export default React.createClass({
  //mixins: [ReactFireMixin],
  getInitialState() {
    return {
      songs: SongStore.getAll()
    }
  },

  componentWillMount() {
    //var firebaseRef = new Firebase('https://mgdp-rebirth-react.firebaseio.com/');
    //this.bindAsArray(firebaseRef, 'items');
    SongActions.loadAll();
    SongStore.addChangeListener(this._onChange);
  },

  componentDidMount() {
    //SoundcloudAPI.authenticate();
  },

  componentWillUnmount() {
    SongStore.removeChangeListener(this._onChange);
  },

  _onChange() {
    this.setState(this.getInitialState());
  },

  render() {
    console.log(this.state);

    return (
      <div style={{marginTop: 51, height: window.innerHeight - 51}}>
        <Header songs={this.state.songs} />
        <RouteHandler songs={this.state.songs} />
      </div>
    );
  }
});

