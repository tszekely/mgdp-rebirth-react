import React from 'react';
import Router from 'react-router';
let {RouteHandler} = Router;

import { Grid, Col, Row, Carousel, CarouselItem } from 'react-bootstrap';

let smCtrl;

export default React.createClass({
  componentDidMount() {
    smCtrl = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: window.innerHeight}});

    new ScrollMagic.Scene()
      .setTween("#home-carousel img", {y: "80%", ease: Linear.easeNone})
      .addTo(smCtrl);
  },

  render() {
    return (
      <Grid fluid className="no-padding-left no-padding-right">
        <Carousel id="home-carousel"
                  className="no-margin-top"
                  prevIcon={<i className="icon-prev ion-ios-arrow-left" />}
                  nextIcon={<i className="icon-next ion-ios-arrow-right" />}
          >
          <CarouselItem>
            <img src="app/img/new_york_city_at_night_lights-wallpaper-1920x1080.jpg" alt="NYC" />
            <div className="carousel-caption">
              <h2>Welcome to the <span className="text-info">React side</span></h2>
              <p>We have JSX and plenty of headaches for you :)</p>
            </div>
          </CarouselItem>
          <CarouselItem>
            <img src="app/img/new_york_city_at_night_3-wallpaper-1920x1080.jpg" alt="NYC" />
            <div className="carousel-caption">
              <h2>Welcome to the <span className="text-info">React side</span></h2>
              <p>We have JSX and plenty of headaches for you :)</p>
            </div>
          </CarouselItem>
        </Carousel>
        <div style={{height: 2000}}></div>
      </Grid>
    );
  }
});

