import React from 'react';
import Router, {Link} from 'react-router';
let {RouteHandler} = Router;

import { Navbar, Nav, Badge } from 'react-bootstrap';
import { NavItemLink } from 'react-router-bootstrap';

export default React.createClass({
  render() {
    return (
      <Navbar fixedTop
              brand={<Link to='/'><img src="app/img/mgdp-logo.svg" alt="Megiddo Productions" style={{height: 40, margin: '-10px 0'}} /></Link>}>
        <Nav>
          <NavItemLink to='tracks'>
            Tracks <Badge bsStyle="primary">{this.props.songs.length > 0 ? this.props.songs.length : null}</Badge>
          </NavItemLink>
        </Nav>
      </Navbar>
    );
  }
});

