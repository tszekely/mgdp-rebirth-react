import React from 'react';
import {Panel,Row,Col,Button,Modal,Input} from 'react-bootstrap';

import SongActions from '../../actions/view/SongActions.js';

export default React.createClass({
  addNewSong() {
    let song = {
      id: parseInt(document.forms.add_song.song_id.value, 10),
      name: document.forms.add_song.song_name.value,
      url: document.forms.add_song.song_url.value
    };

    SongActions.addSong(song);
    this.props.onHide();
  },

  render() {
    return (
      <Modal show={this.props.showModal} onHide={this.props.onHide}>
        <Modal.Header closeButton>
          <Modal.Title>Add new song</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form name="add_song">
            <Input type="text" label="ID *: " ref="songId" name="song_id" value={this.props.lastSong ? this.props.lastSong.id + 1 : 1} disabled required />
            <Input type="text" label="Name *: " ref="songName" name="song_name" required />
            <Input type="url" label="Link to mp3 file *: " ref="songUrl" name="song_url" required />
          </form>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.props.onHide}>Close</Button>
          <Button bsStyle="primary" onClick={this.addNewSong}>Add new song</Button>
        </Modal.Footer>
      </Modal>
    );
  }
});
