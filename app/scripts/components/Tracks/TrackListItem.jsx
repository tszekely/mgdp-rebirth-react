import React from 'react';
import { Panel, Row, Col, Button, ButtonGroup, ProgressBar } from 'react-bootstrap';

import SongActions from '../../actions/view/SongActions.js';

export default React.createClass({
  getInitialState() {
    return {
      duration: null,
      buffered: 0,
      timePlayed: 0,
      isPlaying: false
    };
  },

  componentDidMount() {
    let audio = React.findDOMNode(this.refs['audio' + this.props.song.id]);
    audio.addEventListener('durationchange', this.setDuration);
    audio.addEventListener('timeupdate', this.updateSeeker);
    audio.addEventListener('progress', this.updateBuffered);
  },

  onPlay() {
    let audio = React.findDOMNode(this.refs['audio' + this.props.song.id]);
    audio.play();
    this.setState({
      isPlaying: true
    });
  },

  onPause() {
    let audio = React.findDOMNode(this.refs['audio' + this.props.song.id]);
    audio.pause();
    this.setState({
      isPlaying: false
    });
  },

  onStop() {
    let audio = React.findDOMNode(this.refs['audio' + this.props.song.id]);
    audio.currentTime = 0;
    audio.pause();
    this.setState({
      isPlaying: false
    });
  },

  onRemoveSong() {
    let songId = this.props.song.id;
    SongActions.deleteSong(songId);
  },

  setDuration(e) {
    this.setState({
      duration: e.target.duration
    });
  },

  updateSeeker(e) {
    this.setState({
      timePlayed: e.target.currentTime
    });
  },

  updateBuffered(e){
    this.setState({
      buffered: e.target.buffered.end(e.target.buffered.length -1)
    });
  },

  calculateBuffer() {
    return this.state.duration ? (this.state.buffered / this.state.duration) * 100 : 0;
  },

  calculateProgress() {
    return this.state.duration ? (this.state.timePlayed / this.state.duration) * 100 : 0;
  },

  formatTime(seconds) {
    return `${parseInt(seconds / 60)}:${parseInt(seconds % 60)}`;
  },

  render() {
    let song = this.props.song;

    let footer = (
      <div className="text-center">
        <ButtonGroup>
          <Button bsStyle="primary" onClick={this.onPlay}>
            <i className="ion-ios-play" />
          </Button>
          <Button bsStyle="info" onClick={this.onPause}>
            <i className="ion-ios-pause" />
          </Button>
          <Button bsStyle="danger" onClick={this.onStop}>
            <i className="ion-stop" />
          </Button>
        </ButtonGroup>
        <Button bsStyle="danger" className="pull-right" onClick={this.onRemoveSong}><i className="ion-ios-trash-outline" /></Button>
      </div>
    );

    return (
      <Row key={song.id}>
        <Col xs={12} md={10} mdOffset={1}>
          <Panel bsStyle="primary" header={song.name} footer={footer} className="track">
            <Row>
              <Col xs={6}>{this.formatTime(this.state.timePlayed)}</Col>
              <Col xs={6} className="text-right">{this.formatTime(this.state.duration)}</Col>
            </Row>
            <ProgressBar className="relative">
              <ProgressBar bsStyle="info" now={this.calculateBuffer()} className="absolute transition-all-100" />
              <ProgressBar active={this.state.isPlaying} now={this.calculateProgress()} className="absolute transition-all-100" />
            </ProgressBar>
            <audio controls="false" style={{width: '100%'}} ref={'audio' + song.id} onTimeUpdate={this.onUpdateSeeker} className="hidden">
              <source src={song.url} type="audio/mpeg" />
              Your browser does not support the audio element.
            </audio>
          </Panel>
        </Col>
      </Row>
    );
  }
});
