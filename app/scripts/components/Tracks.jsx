import React from 'react';
import Router from 'react-router';
let {RouteHandler} = Router;
import { Grid, Col, Row, Button } from 'react-bootstrap';

import TrackListItem from './Tracks/TrackListItem.jsx';
import AddTrackModal from './Tracks/AddTrackModal.jsx';

export default React.createClass({
  getInitialState() {
    return {
      showAddModal: false
    }
  },

  closeAddModal() {
    this.setState({ showAddModal: false });
  },

  openAddModal() {
    this.setState({ showAddModal: true });
  },

  render() {
    let songs = this.props.songs;

    let tracklistItems = songs.map((song)=>{
      return (<TrackListItem song={song} />);
    });

    return (
      <Grid>
        <Row>
          <Col sm={3} smOffset={8} style={{marginTop: 50}}>
            <Button block bsStyle="success" onClick={this.openAddModal}>
              <i className="ion-ios-plus-outline" /> Add track
            </Button>
          </Col>
        </Row>
        <div style={{marginTop: 50}}>
          {tracklistItems}
        </div>
        <AddTrackModal showModal={this.state.showAddModal} onHide={this.closeAddModal} lastSong={songs[songs.length - 1]} />
      </Grid>
    );
  }
});

