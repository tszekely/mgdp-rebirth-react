/**
 * Created by Timotheus on 7/27/2015.
 */

import 'babel/polyfill';

import React from 'react';
import Router from 'react-router';
let {Route, DefaultRoute, NotFoundRoute, Redirect} = Router;

import AppHandler from './components/App.jsx';
import HomeHandler from './components/Home.jsx';
import TracksHandler from './components/Tracks.jsx';

let App = React.createClass({
  render () {
    return (
      <h1>Hello world</h1>
    );
  }
});

const routes = (
  <Route handler={AppHandler}>
    <DefaultRoute handler={HomeHandler} />
    <Route name='tracks' path='/tracks' handler={TracksHandler} />
  </Route>
);

Router.run(routes, Router.HistoryLocation, (Handler) => {
  React.render(<Handler />, document.getElementById('mgdp-app'));
});

//React.render(<App />, document.getElementById('mgdp-app'));
