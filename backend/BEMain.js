var express = require('express');
var bodyParser = require('body-parser');
var fs = require('fs');
var cors = require('cors');
var _ = require('lodash');

var songs = [];

var app = express();

app.use(cors());

app.use(bodyParser.json());
//
//app.use(function(req, res) {
//  res.sendfile(__dirname + '/Public/index.html');
//});

app.use('/',express.static( './../' ));

app.get('/songs/', function (req, res) {
  console.log(songs);
  res.send(songs);
});

app.post('/songs/', function (req, res) {
  songs.push(req.body);

  fs.writeFile('./backend/data.json', JSON.stringify(songs));

  res.send(songs);
});

app.delete('/songs/:id', function (req, res) {
  var songId = req.params.id;

  songs = _.filter (songs, function (song) {
    return parseInt(song.id, 10) !== parseInt(songId, 10);
  });

  console.log(songId, songs);

  res.send(songs);
});

fs.readFile('./backend/data.json', 'utf8', function (err,data) {
  if (err) {
    return console.log(err);
  }
  songs = JSON.parse(data);
  console.log("File read: ", data);

  var server = app.listen(3000, function () {
    var host = server.address().address;
    var port = server.address().port;

    console.log('Example app listening at http://%s:%s', host, port);
  });
});

process.stdin.resume();//so the program will not close instantly

function exitHandler(options, err) {
  fs.writeFileSync('./backend/data.json', JSON.stringify(songs));

  if(err) {
    return console.log(err);
  }

  console.log("The file was saved: ", JSON.stringify(songs));

  if (err) console.log(err.stack);
  if (options.exit) process.exit();
}

//do something when app is closing
process.on('exit', exitHandler.bind(null,{cleanup:true}));

//catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, {exit:true}));

//catches uncaught exceptions
process.on('uncaughtException', exitHandler.bind(null, {exit:true}));
